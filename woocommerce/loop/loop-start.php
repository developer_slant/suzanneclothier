<?php

/**
 * Product Loop Start
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.3.0
 */

if (!defined('ABSPATH')) {
  exit;
}
?>
<div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-8">